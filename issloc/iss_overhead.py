#!/usr/bin/python3
"""Alta3 Research | <your name here>
   Using an HTTP GET to determine when the ISS will pass over head"""

# pprint makes dictionaries a lot more human readable
from pprint import pprint

# python3 -m pip install requests
import requests

A = input("Enter latitude:");
B = input("Enter longitude:");

# define base URL
#URL = "http://api.open-notify.org/iss-pass.json?lat=47.6&lon=-122.3"

URL1 = "http://api.open-notify.org/iss-pass.json?lat=" + A + "&lon=" +B;

print(URL1)

def main():
    if(A == '47.6' and B == '-122.3'):
        # SWAPI response is stored in "resp" object
         resp= requests.get(URL1)

         # check to see if the status is anything other than what we want, a 200 OK
         if resp.status_code == 200:
              # convert the JSON content of the response into a python dictionary
              vader= resp.json()
              pprint(vader)

         else:
             print("That is not a valid URL.")
    else:
        print("Entered Latitude and Longitude is wrong. Please try again....")
    
    # stuck? you can always write comments
    # Try describe the steps you would take manually



if __name__ == "__main__":
    main()

