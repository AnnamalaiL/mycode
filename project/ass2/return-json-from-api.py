#!/usr/bin/python3
# Author - Annamalai Lakshmanan
# Version 1
# Code Name - Demonstration with the requests HTTP library and displaying in the Human readable format

import requests
import pprint

## Define 2 URL's
MarsRoverPhotos = "https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?sol=1000&page="
Techtransfer = "https://api.nasa.gov/techtransfer/patent/?engine&"

# this function grabs our credentials
def returncreds():
    ## Grab my credentials
    with open("/home/student/nasa.creds", "r") as mycreds:
        nasacreds = mycreds.read()
    ## remove any newline characters from the api_key
    nasacreds = "api_key=" + nasacreds.strip("\n")
    return nasacreds

# main function
def main():
    ## first grab credentials
    nasacreds = returncreds()

    print("======= GOING TO RETRIEVE DATA FROM 2 APIs - MARS ROVER PHOTOS AND TECH TRANSFER ========")
    ## the below is the page number obtained from the user end for which the data needs to be retrieved from Mars Rover Photos
    page = input("Enter the page number which the data needs to be retrieved from Mars Rover Photos API - ")

    # make a request with the request library
    marsroverphotos = requests.get(MarsRoverPhotos + page + "&" + nasacreds)
    techtransfer = requests.get(Techtransfer + nasacreds)

    # strip off json attachment from the response
    marsroverpic = marsroverphotos.json()
    techtransferdata = techtransfer.json()

    ## display NASA's Marsrover data
    print("=============================================")
    print("============== Mars Rover Photos ============")
    pprint.pprint(marsroverpic)
    print("=============================================")
    print("============== Tech Transfer ================")
    pprint.pprint(techtransferdata)
    print("=============================================")
    print("=============================================")

if __name__ == "__main__":
    main()
